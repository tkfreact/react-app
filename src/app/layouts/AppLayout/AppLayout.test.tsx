import * as React from 'react';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import {Header} from 'components/app/Header';
import AppLayout from './AppLayout';

describe('<AppLayout />', () => {
    it('renders <Header /> component', () => {
        const appLayout = shallow(<AppLayout />);
        expect(appLayout.find(Header)).to.exist;
    });
});