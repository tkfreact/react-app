import * as React from 'react';
import {ReactNode} from 'react';
import {Header} from 'components/app/Header';
import {IAppLayoutProps} from './types';
import './styles.scss';

export default class AppLayout extends React.Component<IAppLayoutProps> {
    /**
     * Render component
     *
     * @returns {React.ReactNode} React node
     */
    public render(): ReactNode {
        const {children} = this.props;

        return (
            <>
                <Header />
                <section className="react-app_content">
                    {children}
                </section>
            </>
        );
    }
}