import {Action, handleActions} from 'redux-actions';
import {combineReducers} from 'redux';
import {TestActions} from './actions';
import {IStateTest} from './types';

// Listen for get events
const getActions = handleActions<any, any>(
    {
        [`${TestActions.GET}_SUCCESS`]: (state: IStateTest, action: Action<any>): IStateTest => (action.payload),
    },
    null,
);

export default combineReducers({
    data: getActions,
});