import {createAction} from 'redux-actions';

export enum TestActions {
    GET = 'TEST_GET',
}

export const getTest = createAction(
    TestActions.GET,
    () => {
    },
);