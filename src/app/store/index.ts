import 'babel-polyfill';
import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {routerReducer} from 'react-router-redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import routerMiddleware from 'middleware/router';
import sagaMiddleware from 'middleware/saga';
import sagas from 'middleware/saga/contexts';
import storeReducers from './reducers';

const reducers = combineReducers({
    ...storeReducers,
    routing: routerReducer,
});

// Middlewares
const middlewares = [
    routerMiddleware,
    sagaMiddleware,
    thunkMiddleware,
];

// Store
const store = createStore(
    reducers,
    composeWithDevTools(applyMiddleware(...middlewares)),
);

// Redux SAGA
sagaMiddleware.run(sagas);

export default store;