import {IStateTest} from './reducers/test/types';

export interface IState {
    test: IStateTest
}