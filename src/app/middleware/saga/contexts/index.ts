import {all, fork} from 'redux-saga/effects';
import test from './test';

const sagas = [
    test,
];

export default function* () {
    yield all(sagas.map(saga => fork(saga)));
}