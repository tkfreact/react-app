import {createAction} from 'redux-actions';
import {put, takeEvery} from 'redux-saga/effects';
import {TestActions} from 'store/reducers/test/actions';

/**
 * Called async as would make a call out via service
 *
 * @returns {Generator} Generator function
 */
function* getTestAsync(): Generator {
    return yield fetch(TestActions.GET);
}

/**
 * Generator function would be abstract and call out to services.
 *
 * @param {string} action Action to append to
 * @returns {Generator} This generator
 */
function* fetch(action: string): Generator {
    return yield put(
        createAction(
            `${TestActions.GET}_SUCCESS`,
            () => ('We have some data in redux!'),
        )(),
    );
}

export default function* (): Generator {
    yield takeEvery(TestActions.GET, getTestAsync);
}