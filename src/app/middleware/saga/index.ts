import createSagaMiddleware from 'redux-saga';
import {AllEffect} from 'redux-saga/effects';
import context from './contexts';

export default createSagaMiddleware<() => IterableIterator<AllEffect>>({
    context,
});
