export interface IReduxViewBaseProps {
    data: any
}

export interface IReduxViewStateProps {
    data: any
}

export interface IReduxViewDispatchProps {
    getTest: () => void;
}

export interface IReduxViewProps extends IReduxViewBaseProps,
    IReduxViewStateProps,
    IReduxViewDispatchProps {

}