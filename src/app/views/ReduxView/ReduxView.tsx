import * as React from 'react';
import {ReactNode} from 'react';
import {Button, Grid} from 'semantic-ui-react';
import {IReduxViewProps} from './types';
import connect from './connect';

export class ReduxView extends React.Component<IReduxViewProps> {
    /**
     * Render component
     *
     * @returns {React.ReactNode} React node
     */
    public render(): ReactNode {
        const {data, getTest} = this.props;
        return (
            <Grid columns={2}>
                <Grid.Column>
                    <h1>Redux example</h1>
                    <Button onClick={getTest}>Populate store!</Button>
                </Grid.Column>

                <Grid.Column>
                    Data:
                    <p>{data && data.toString()}</p>
                </Grid.Column>
            </Grid>
        );
    }
}

export default connect(ReduxView);