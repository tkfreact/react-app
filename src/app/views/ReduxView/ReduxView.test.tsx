import * as React from 'react';
import {Button} from 'semantic-ui-react';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import {ReduxView} from './ReduxView';

describe('<ReduxView />', () => {
    it('renders header', () => {
        const reduxView = shallow(<ReduxView data={null} getTest={() => {
        }} />);
        const header = reduxView.find('h1');

        expect(header).to.exist;
        expect(header.text()).to.equal('Redux example');
    });

    it('renders a button', () => {
        const reduxView = shallow(<ReduxView data={null} getTest={() => {
        }} />);
        const button = reduxView.find(Button);

        expect(button).to.exist;
    });
});