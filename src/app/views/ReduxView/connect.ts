import {connect} from 'react-redux';
import {IState} from 'store/types';
import {getTest} from 'store/reducers/test/actions';
import {IReduxViewBaseProps, IReduxViewDispatchProps, IReduxViewStateProps} from './types';

const mapStateToProps = (state: IState): IReduxViewStateProps => ({
    data: state.test.data,
});

const mapDispatchToProps = (dispatch: Function): IReduxViewDispatchProps => ({
    getTest() {
        dispatch(getTest());
    },
});

export default connect<IReduxViewStateProps, IReduxViewDispatchProps, IReduxViewBaseProps>(
    mapStateToProps,
    mapDispatchToProps,
);

