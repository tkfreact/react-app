import * as React from 'react';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import IndexView from './IndexView';

describe('<IndexView />', () => {
    it('renders hello world message', () => {
        const indexView = shallow(<IndexView />);
        const tag = indexView.find('strong');

        expect(tag).to.exist;
        expect(tag.text()).to.equal('Hello World!');
    });
});