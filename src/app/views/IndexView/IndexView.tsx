import * as React from 'react';
import {ReactNode} from 'react';
import {IIndexViewProps} from './types';

export default class IndexView extends React.Component<IIndexViewProps> {
    /**
     * Render component
     *
     * @returns {React.ReactNode} React node
     */
    public render(): ReactNode {
        return (
            <strong>Hello World!</strong>
        );
    }
}