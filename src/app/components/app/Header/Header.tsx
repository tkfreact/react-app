import * as React from 'react';
import {ReactNode} from 'react';
import {push} from 'react-router-redux';
import {Menu, MenuItemProps} from 'semantic-ui-react';
import {IHeaderProps} from './types';
import store from 'store';
import './styles.scss';

export default class Header extends React.Component<IHeaderProps> {

    /**
     * Render component
     *
     * @returns {React.ReactNode} React node
     */
    public render(): ReactNode {
        return (
            <header className="react-app__header">
                <Menu inverted={true} secondary={true} fixed="top">
                    {this.renderMenuItems()}
                </Menu>
            </header>
        );
    }

    /**
     * Build menu config
     *
     * @returns {Array<MenuItemProps>} Array of menu item props
     */
    private getMenuConfig = (): Array<MenuItemProps> => [
        {
            name: 'React App',
            onClick: () => store.dispatch(push('/')),
            className: 'react-app__header_item--branded',
        }, {
            name: 'Redux example',
            onClick: () => store.dispatch(push('/redux')),
        },
    ];

    /**
     * Render menu item component
     *
     * @param {MenuItemProps} props Menu item props
     * @returns {React.ReactNode} Menu item component
     */
    private renderMenuItem = (props: MenuItemProps): ReactNode => (
        <Menu.Item key={props.name} className="react-app__header_item" {...props} />
    );

    /**
     * Map config to component
     *
     * @returns {Array<React.ReactNode>} Menu item components
     */
    private renderMenuItems = (): Array<ReactNode> => this.getMenuConfig().map(this.renderMenuItem);
}