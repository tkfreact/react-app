import * as React from 'react';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import {Menu} from 'semantic-ui-react';
import Header from './Header';

describe('<Header />', () => {
    it('renders 2 <MenuItem /> links', () => {
        const header = shallow(<Header />);
        expect(header.find(Menu.Item)).to.have.length(2);
    });
});