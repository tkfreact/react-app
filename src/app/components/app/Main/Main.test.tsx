import * as React from 'react';
import {Route} from 'react-router-dom';
import {expect} from 'chai';
import {shallow} from 'enzyme';
import Header from './Main';

describe('<Main />', () => {
    it('renders 2 <Route /> components', () => {
        const main = shallow(<Header />);
        expect(main.find(Route)).to.have.length(2);
    });
});