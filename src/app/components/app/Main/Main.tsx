import * as React from 'react';
import {ReactNode} from 'react';
import {Switch} from 'react-router-dom';
import routes from 'routes';
import {AppLayout} from 'layouts/AppLayout';
import {IMainProps} from './types';

export default class Main extends React.Component<IMainProps> {
    /**
     * Render component
     *
     * @returns {React.ReactNode} Main app react component
     */
    public render(): ReactNode {
        return (
            <AppLayout>
                <Switch>
                    {routes}
                </Switch>
            </AppLayout>
        );
    }
}