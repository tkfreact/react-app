import {RouteProps} from 'react-router';
import {IndexView} from 'views/IndexView';
import {ReduxView} from 'views/ReduxView';

const config: Array<RouteProps> = [
    {
        component: IndexView,
        path: '/',
        exact: true,
    },
    {
        component: ReduxView,
        path: '/redux',
        exact: true,
    },
];

export default config;