import * as React from 'react';
import {ReactNode} from 'react';
import {Route, RouteProps} from 'react-router-dom';
import config from './config';

/**
 * Build a route from props
 *
 * @param {RouteProps} route Route props to spread
 * @returns {React.ReactNode} React node
 */
function getRoute(route: RouteProps): ReactNode {
    return (
        <Route
            key={route.path}
            {...route}
        />
    );
}

/**
 * Build routes from configuration
 *
 * @param {Array<RouteProps>} config An array of route props
 * @returns {Array<React.ReactNode>} Array of react nodes
 */
function getRoutes(config: Array<RouteProps>): Array<ReactNode> {
    return config.map(getRoute);
}

export default getRoutes(config);