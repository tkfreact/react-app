import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {hot} from 'react-hot-loader';
import {ConnectedRouter} from 'react-router-redux';
import {Main} from './components/app/Main/index';
import {history} from './middleware/router';
import store from './store';
// Import semantic styles
import 'semantic-ui-css/semantic.min.css';

// Render app to DOM
ReactDOM.render(
    hot(module)(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Main />
            </ConnectedRouter>
        </Provider>,
    ),
    document.getElementById('app') as HTMLDivElement,
);