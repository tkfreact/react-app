// Base requires
const path = require('path');

// Plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Paths
const paths = {
    js: path.resolve(__dirname, 'src/app'),
    src: path.resolve(__dirname, 'src'),
    dist: path.resolve(__dirname, 'dist'),
};

module.exports = {
    mode: 'development',
    entry: path.join(paths.js, 'index.tsx'),
    output: {
        path: paths.dist,
        filename: 'bundle.js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(paths.src, 'index.html'),
        }),
    ],
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
        modules: [
            path.resolve('./src/app'),
            path.resolve('./node_modules'),
        ],
    },
    module: {
        rules: [
            // Typescript rules
            {
                test: /\.(ts|tsx)?$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            babelrc: true,
                            plugins: ['react-hot-loader/babel'],
                        },
                    },
                    'awesome-typescript-loader',
                    // 'eslint-loader',
                ],
            },

            // SASS rules
            {
                test: /\.(scss|css)$/,
                use: [
                    {loader: 'style-loader'},
                    {
                        loader: 'css-loader', options: {
                            url: false,
                        },
                    },
                    {loader: 'sass-loader'},
                ],
            },
        ],
    },
};