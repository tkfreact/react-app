# React App Example

This repo contains a small react app (WIP) that acts as a skeleton for a React/Redux/Saga project.

## Installation and run
Run `npm i`.

To view development build run `npm start`.

### Redux example
The redux example page shows data being populated into redux via `redux-actions` and saga actions.